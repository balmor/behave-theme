# Behave Theme

A port of [Christian Petersen' Textmate Behave theme](https://github.com/fnky/behave-theme).

An easy-on-the-eyes dark syntax theme for Visual Studio Code.

I also recommend use [JavaScript Atom Grammar](https://marketplace.visualstudio.com/items?itemName=ms-vscode.js-atom-grammar) extension.

## Screenshot

<img src="https://gitlab.com/balmor/behave-theme/raw/master/screenshots/behave-preview.png" width="900" alt="Behave in action" />

## More Information
* [GitHub repository](https://gitlab.com/balmor/behave-theme).